FROM python:3.4

MAINTAINER Elliot Smith (elliot.smith91@gmail.com) 

# make sure we're up to date
RUN apt-get update --fix-missing
RUN apt-get install -y build-essential git
RUN apt-get install -y libyaml-dev 

#RUN pip install -r /opt/venv/requirements.txt

